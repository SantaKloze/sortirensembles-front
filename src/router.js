import Vue from 'vue'
import Router from 'vue-router'
import Store from './store'
import vLoginForm from '@/components/vLoginForm.vue'
import vRegisterForm from '@/components/vRegisterForm.vue'
import cities from '@/views/cities.vue'
import sites from '@/views/sites.vue'
import home from '@/components/home.vue'
import profile from '@/views/userProfile.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: vLoginForm
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/register',
      name: 'Register',
      component: vRegisterForm
    },
    {
      path: '/cities',
      name: 'Villes',
      component: cities
    },
    {
      path: '/sites',
      name: 'Sites',
      component: sites
    },
    {
      path: '/profile',
      name: 'User Profile',
      component: profile
    }
  ]
})

router.beforeEach((to, from, next) => {
  console.log(Vue.prototype.$session.get('userToken'))
  if (Vue.prototype.$session.get('userToken')) {
    Store.commit('connect', true)
  }
  else {
    Store.commit('connect', false)
    Vue.prototype.$session.set('userToken', false)
  }
  next();
})

export default router
